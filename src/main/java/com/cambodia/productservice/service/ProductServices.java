package com.cambodia.productservice.service;

import com.cambodia.productservice.dto.ProductRequest;
import com.cambodia.productservice.dto.ProductResponse;
import com.cambodia.productservice.mapper.ProductMapper;
import com.cambodia.productservice.model.Product;
import com.cambodia.productservice.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ProductServices {

    private final ProductRepository productRepository;
    public void createProduct(ProductRequest productRequest){
        // create new product object using with builder.
        Product product = Product.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .build();

        productRepository.save(product);
        log.info("product {} has save to database!", product.getId());
    }

    public List<ProductResponse> getAllProduct(){
        List<Product> products = productRepository.findAll();

        return products.stream()
                .map(ProductMapper::mapToProductResponse)
                .toList();
    }
}
