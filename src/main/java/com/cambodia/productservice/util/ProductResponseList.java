package com.cambodia.productservice.util;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Builder
@NoArgsConstructor
@AllArgsConstructor
/*@JsonSerialize*/
@Data
public class ProductResponseList<T> {
    private String message;
    private List<T> data;
}
