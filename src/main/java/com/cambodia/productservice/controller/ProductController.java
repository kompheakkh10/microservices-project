package com.cambodia.productservice.controller;

import com.cambodia.productservice.constant.Message;
import com.cambodia.productservice.dto.ProductRequest;
import com.cambodia.productservice.dto.ProductResponse;
import com.cambodia.productservice.service.ProductServices;
import com.cambodia.productservice.util.ProductResponseList;
import com.cambodia.productservice.util.Response;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/api/product")
public class ProductController {

    private final ProductServices productServices;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Response<?>> createProduct(@RequestBody ProductRequest productRequest){
        productServices.createProduct(productRequest);

        ProductResponse productResponseObject = ProductResponse.builder()
                .name(productRequest.getName())
                .description(productRequest.getDescription())
                .price(productRequest.getPrice())
                .build();

        Response<ProductResponse> productResponse = new Response<>();
        productResponse.setMessage(Message.MESSAGE);
        productResponse.setData(productResponseObject);
        productResponse.setStatus(Message.MESSAGE);

        return ResponseEntity.ok(productResponse);
    }

    @GetMapping()
    public ResponseEntity<ProductResponseList<ProductResponse>> getAllProduct(){
        List<ProductResponse> productResponses = productServices.getAllProduct();

        ProductResponseList<ProductResponse> productResponseResponse = ProductResponseList.<ProductResponse>builder()
                .message(Message.MESSAGE)
                .data(productResponses)
                .build();
        return ResponseEntity.ok(productResponseResponse);
    }
}
