package com.cambodia.productservice.mapper;

import com.cambodia.productservice.dto.ProductResponse;
import com.cambodia.productservice.model.Product;

public class ProductMapper {

    public static ProductResponse mapToProductResponse(Product product){

        return ProductResponse.builder()
                .name(product.getName())
                .price(product.getPrice())
                .description(product.getDescription())
                .build();
    }
}
